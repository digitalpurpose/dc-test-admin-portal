import {AbstractControl, FormGroup} from '@angular/forms';

export const trimStringsInFormGroup = (formGroup: FormGroup, settings?: TrimFormControlSettings) => {
  Object.keys(formGroup.controls).forEach(key => {
    if (!settings || !settings?.excludes || settings?.excludes.findIndex(str => str == key) == -1) {
      const control: AbstractControl = formGroup.get(key);
      if (typeof control.value == 'string' && (control.value as string).length) {
        const emitEvent = settings?.emitEvent || false;
        control.patchValue((control.value as string).trim(), {emitEvent: emitEvent});
      }
    }
  })
}

export interface TrimFormControlSettings {
  emitEvent?: boolean; // Defaults to false
  excludes?: string[]; // List of control names
}
