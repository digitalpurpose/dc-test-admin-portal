import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {finalize} from 'rxjs/operators';
import {trimStringsInFormGroup} from '../../../util/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  form: FormGroup;
  error: any;
  loading = false;
  success = false;

  constructor(private fb: FormBuilder, private authService: AuthService) {
    this.form = this.fb.group({
      email: ['', Validators.required],
    });
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.loading = true;
    trimStringsInFormGroup(this.form);
    this.form.disable();
    this.authService.forgotPassword(this.form.value)
      .pipe(finalize(() => {
        this.form.enable();
        this.loading = false;
      }))
      .subscribe(
        (response) => this.success = true,
        (error) => {
          if (error.code == 'validation_error') {
            this.error = this.extractError(error);
          } else {
            // TODO success message indicates we should ignore case where user doesn't exist
            this.success = true;
            // this.error = error;
          }
        }
      );
  }

  extractError(error) {
    const response = {code: error.code, message: 'Some data is missing or incorrect', field: {}};
    if (error.fieldErrors) {
      for (let i = 0; i < error.fieldErrors.length; i++) {
        response.field[error.fieldErrors[i].field] = error.fieldErrors[i].message;
      }
    }
    return response;
  }

}
