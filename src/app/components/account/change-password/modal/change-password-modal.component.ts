import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-change-password-modal',
  templateUrl: './change-password-modal.component.html',
  styleUrls: ['./change-password-modal.component.scss']
})
export class ChangePasswordModalComponent implements OnInit {

  constructor(private activeModal: NgbActiveModal) {

  }

  ngOnInit(): void {
  }

  cancel() {
    this.activeModal.dismiss();
  }

  close() {
    this.activeModal.close();
  }
}
