import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {finalize} from 'rxjs/operators';
import {AuthService} from 'src/app/services/auth.service';
import {UserService} from 'src/app/services/user.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  form: FormGroup;
  passwordStrength = 0;
  error: any;
  showFormInvalid: boolean;
  saving: boolean;
  minPasswordLength = 8;

  @Input() requireOldPassword = true;
  @Input() confirmationToken: string;
  @Input() showCancel = true;

  @Output() onSuccess = new EventEmitter<boolean>();
  @Output() onCancel = new EventEmitter<boolean>();
  @Output() onError = new EventEmitter<any>();

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private authService: AuthService) {

  }

  ngOnInit(): void {
    this.form = this.fb.group({
      oldPassword: [''],
      password: ['', [Validators.required, Validators.minLength(this.minPasswordLength), Validators.maxLength(100)]],
      // tslint:disable-next-line:max-line-length
      confirmationPassword: ['', [this.passwordConfirmationValidator(), Validators.required, Validators.minLength(this.minPasswordLength), Validators.maxLength(100)]],
      confirmationToken: [this.confirmationToken]
    });

    if (this.requireOldPassword) {
      this.form.controls.oldPassword.setValidators(Validators.required);
      this.form.controls.oldPassword.updateValueAndValidity();
    }
  }

  hasError(field: any) {
    const control = this.form.controls[field];
    if (control && this.showFormInvalid && this.form.invalid && control.invalid) {
      return true;
    } else if (this.error && this.error.field) {
      return !!this.error.field[field];
    }
    return false;
  }

  passwordConfirmationValidator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const form = control.parent;
      if (form) {
        const password = form.get('password');
        const match = password.value == control.value;
        return match ? null : {passwordConfirmationError: true};
      }
      return null;
    };
  }

  isControlDirty(field: string) {
    return this.form.get(field).invalid && this.form.get(field).dirty;
  }

  onSubmit() {
    if (this.saving) {
      return;
    }
    if (this.form.invalid) {
      this.showFormInvalid = true;
      // this.error = 'Some data is missing or incorrect';
      return;
    }
    this.saving = true;
    this.error = null;
    this.showFormInvalid = false;
    this.form.disable();
    const observable = this.confirmationToken ? this.authService.resetForgottenPassword(this.form.value) : this.userService.changePassword(this.form.value);
    observable.pipe(finalize(() => {
      this.form.enable();
      this.saving = false;
    })).subscribe(
      (response) => { this.onSuccess.emit(true); },
      (error) => {
        if (error.code == 'validation_error') {
          this.error = this.extractError(error);
          this.showFormInvalid = true;
        } else {
          this.error = error;
          this.onError.emit(error);
        }
      }
    );
  }

  cancel() {
    this.onCancel.emit(true);
  }

  extractError(error) {
    const response = {code: error.code, message: 'Some data is missing or incorrect', field: {}};
    if (error.fieldErrors) {
      for (let i = 0; i < error.fieldErrors.length; i++) {
        response.field[error.fieldErrors[i].field] = error.fieldErrors[i].message;
      }
    }
    return response;
  }
}
