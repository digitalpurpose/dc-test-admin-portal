import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from "../../../services/auth.service";
import {finalize} from "rxjs/operators";

@Component({
  selector: 'app-reset-forgotten-password',
  templateUrl: './reset-forgotten-password.component.html',
  styleUrls: ['./reset-forgotten-password.component.scss']
})
export class ResetForgottenPasswordComponent implements OnInit {

  loading = true;
  confirmationToken: string;
  success = false;
  expired = false;

  constructor(private activatedRoute: ActivatedRoute, private authService: AuthService) {
    this.confirmationToken = this.activatedRoute.snapshot.params.confirmationToken;
    this.authService.checkResetForgottenPasswordToken({confirmationToken: this.confirmationToken})
      .pipe(finalize(() => {this.loading = false; }))
      .subscribe(
        (response) => {
          // do nothing
        },
        (error) => {
          this.expired = true;
        });
  }

  ngOnInit(): void {
  }

  onSuccess() {
    this.success = true;
  }

  onError(error) {
    if (error.code === 'invalid_reset_password_token') {
      this.expired = true;
    }
  }

}
