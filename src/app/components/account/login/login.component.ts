import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from '../../../services/auth.service';
import {trimStringsInFormGroup} from '../../../util/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  targetUrl: string;
  loading: boolean;
  error;
  validationMessage: string = '';

  constructor(private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private authService: AuthService) {
    this.form = this.fb.group({
      email: [''],
      password: ['']
    });

    this.route.queryParams.subscribe(params => {
      this.targetUrl = params['target'];
    });
  }

  ngOnInit(): void {
  }

  onSubmit() {
    trimStringsInFormGroup(this.form, {excludes: ['password']});
    const request = this.form.value;
    this.loading = true;
    this.error = null;
    this.authService.login(request).subscribe(
      result => this.rerouteAfterLogin(),
      error => {
       console.log('Error logging in: ', error);
       this.loading = false;
       this.error = error;
      });
  }

  rerouteAfterLogin() {
    if (this.targetUrl) {
      this.router.navigateByUrl(this.targetUrl);
    } else {
      this.router.navigate(['/devices']);
    }
  }
}
