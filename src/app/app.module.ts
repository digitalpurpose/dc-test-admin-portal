import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './components/app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  NgbDateAdapter,
  NgbDateParserFormatter,
  NgbModalConfig,
  NgbModule,
  NgbTimeAdapter
} from '@ng-bootstrap/ng-bootstrap';
import {LocalStorageModule} from 'angular-2-local-storage';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {
  faCaretLeft,
  faCaretRight,
  faCheckCircle,
  faChevronRight,
  faChevronLeft,
  faCog,
  faEdit,
  faExclamationCircle,
  faExclamationTriangle,
  faLock,
  faPencilAlt,
  faRedoAlt,
  faSpinner,
  faTimes,
  faTimesCircle,
  faTrash,
  faUpload,
  faUser,
} from '@fortawesome/free-solid-svg-icons';

import {RequestInterceptor} from './services/request.interceptor';
import {ResponseInterceptor} from './services/response.interceptor';
import {SharedModule} from './modules/shared/shared.module';
import {UsersModule} from './modules/users/users.module';
import {PortalComponent} from './components/portal/portal.component';
import {LoginComponent} from './components/account/login/login.component';
import {ChangePasswordComponent} from './components/account/change-password/change-password.component';
import {ForgotPasswordComponent} from './components/account/forgot-password/forgot-password.component';
import { ChangePasswordModalComponent } from './components/account/change-password/modal/change-password-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    PortalComponent,
    LoginComponent,
    ChangePasswordComponent,
    ChangePasswordModalComponent,
    ForgotPasswordComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    LocalStorageModule.forRoot({prefix: 'dctest-admin'}),
    FontAwesomeModule,
    SharedModule,
    UsersModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ResponseInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(private faIconLibrary: FaIconLibrary,
              private ngbModalConfig: NgbModalConfig) {
    this.faIconLibrary.addIcons(
      faUser, faCog, faCaretLeft, faCaretRight, faLock, faUpload, faTrash,
      faChevronLeft, faChevronRight, faExclamationTriangle,
      faPencilAlt, faExclamationCircle, faSpinner, faEdit, faRedoAlt, faCheckCircle, faTimesCircle,
      faTimes
    );

    // Global Angular Bootstrap modal configuration
    ngbModalConfig.centered = true;
    ngbModalConfig.backdrop = 'static';
    ngbModalConfig.keyboard = false;
    ngbModalConfig.container = 'app-root';
    ngbModalConfig.windowClass = 'ui-themed';
    ngbModalConfig.backdropClass = 'blurred-backdrop';
  }
   
}
