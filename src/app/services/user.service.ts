import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../model/user';
import {Page} from '../model/page';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiUrl = environment.apiUrl + '/user';

  constructor(private http: HttpClient) {
  }

  search(request): Observable<Page<User>> {
    return this.http.post<Page<User>>(`${this.apiUrl}/search`, request);
  }

  get(id: number): Observable<User> {
    return this.http.get<User>(`${this.apiUrl}/${id}`);
  }

  create(request: any): Observable<User> {
    return this.http.post<User>(`${this.apiUrl}`, request);
  }

  update(request: any): Observable<User> {
    return this.http.put<User>(`${this.apiUrl}`, request);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }

  updatePassword(id: number, request): Observable<User> {
    return this.http.put<User>(`${this.apiUrl}/${id}/password`, request);
  }

  unlock(id: number) {
    return this.http.put<User>(`${this.apiUrl}/${id}/unlock`, {});
  }

  changePassword(request: any): Observable<any> {
    return this.http.post(`${this.apiUrl}/change-password`, request);
  }
}
