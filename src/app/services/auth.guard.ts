import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {switchMap} from "rxjs/operators";
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router,
              private authService: AuthService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.isAuthenticated().pipe(
      switchMap(isAuth => {
        if (!isAuth) {
          this.router.navigate(['/login'], {queryParams: {target: state.url}});
          return of(false);
        }

        const user = this.authService._user.value;
        if (!user) {
          this.authService.loadUserDetails();
        }

        return of(true);
      })
    );
  }

}
