import {Injectable} from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, BehaviorSubject, throwError} from "rxjs";
import {Router} from "@angular/router";
import {catchError, filter, take, switchMap} from "rxjs/operators";
import {AuthService} from './auth.service';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {

  private refreshTokenInProgress = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private router: Router, private authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(response => {
      const err = response.error || response;
      if (err.status === 401) {
        // refresh token call fails possibly because the refresh token has expired
        if (request.url.indexOf('auth/refresh') != -1) {
          // force re-login
          this.refreshTokenInProgress = false;
          this.authService.logout().subscribe(() => this.router.navigate(['/login']));
          return throwError(err);
        } else {
           // otherwise try to do a token refresh
           return this.handleRefresh(request, next);
        }
      } else if (err.status === 403) {
        this.router.navigate(['/unauthorised']);
      }
      // Check if the error has a custom code and determine error message
      // const error = err.code ? err : err.message || err.statusText;
      const error = response.status == 0 ? 'We could not process your request' : err;
      return throwError(error);
    }));
  }

  handleRefresh(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
     // if the token is not yet expired, assume this is a legitimate unauthorised and redirect
     if (!this.authService.isTokenExpired()) {
         this.router.navigate(['/unauthorised']);
         return throwError({message: 'Unauthorised action'});
     }
     
     // if token is expired, call refresh
    if (this.refreshTokenInProgress) {
      return this.refreshTokenSubject.pipe(
        filter(result => result !== null),
        take(1),
        switchMap((token: any) => {
           let clonedRequest = request.clone({
               setHeaders: {
                   Authorization: `Bearer ${token}`
               }
           });
           return next.handle(clonedRequest);
        })
      );
    } else {
      this.refreshTokenInProgress = true;
      this.refreshTokenSubject.next(null);
      return this.authService.refreshToken().pipe(
        switchMap((token: any) => {
          this.refreshTokenInProgress = false;
          this.refreshTokenSubject.next(token);
          let clonedRequest = request.clone({
              setHeaders: {
                  Authorization: `Bearer ${token}`
              }
          });
          return next.handle(clonedRequest);
        }),
        catchError((err: any) => {
          this.refreshTokenInProgress = false;
          this.authService.logout().subscribe(() => this.router.navigate(['/login']));
          return throwError(err);
        })
      )
    }
  }
}
