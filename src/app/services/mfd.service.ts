import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Mfd} from '../model/mfd';

@Injectable({
  providedIn: 'root'
})
export class MfdService {

  private apiUrl = environment.apiUrl + '/mfd';

  constructor(private http: HttpClient) {
  }

  get(identifier: string): Observable<Mfd[]> {
    return this.http.get<Mfd[]>(`${this.apiUrl}/${identifier}`);
  }

  create(request: any): Observable<Mfd> {
    return this.http.post<Mfd>(`${this.apiUrl}`, request);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }
}
