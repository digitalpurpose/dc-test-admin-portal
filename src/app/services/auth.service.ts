import {environment} from "../../environments/environment";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable, of} from "rxjs";
import {map, mergeMap} from "rxjs/operators";
import {User} from "../model/user";
import {JwtHelperService} from '@auth0/angular-jwt';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { LocalStorageService } from 'angular-2-local-storage';

const apiUrl = environment.apiUrl + '/auth';

const TOKEN_KEY = "_tok";
const REFRESH_TOKEN_KEY = "_rftok";
const USER_KEY = "_user";
const TOKEN_PRE_EXPIRY_IN_SEC = 60;

@Injectable({providedIn: 'root'})
export class AuthService {

  _user = new BehaviorSubject<User>(null);
  public user$: Observable<User> = this._user.asObservable();
  jwtHelper;

  constructor(private http: HttpClient,
              private localStorage: LocalStorageService,
              private modalService: NgbModal) {
    this.jwtHelper = new JwtHelperService();
    // Attempt to initialise the user when initialising the service
    const user: User = this.localStorage.get(USER_KEY) || null;
    this._user.next(user);
  }

  login(request: { email: string, password: string }): Observable<User> {
    return this.http.post<LoginResponse>(`${apiUrl}`, request).pipe(
      map(result => {
        this.localStorage.set(TOKEN_KEY, result.token);
        this.localStorage.set(USER_KEY, result.user);
        this.localStorage.set(REFRESH_TOKEN_KEY, result.refresh_token);
        this._user.next(result.user);
        return result.user;
      })
    );
  }

  keepAlive(request: { email: string, password: string }): Observable<User> {
    return this.http.post<LoginResponse>(`${apiUrl}/keepalive`, request).pipe(
      map(result => {
        this.localStorage.set(TOKEN_KEY, result.token);
        this.localStorage.set(USER_KEY, result.user);
        this.localStorage.set(REFRESH_TOKEN_KEY, result.refresh_token);
        this._user.next(result.user);
        return result.user;
      })
    );
  }

  logout() {
    // Close all modals
    this.modalService.dismissAll();
    this.localStorage.remove(TOKEN_KEY);
    this.localStorage.remove(USER_KEY);
    this.localStorage.remove(REFRESH_TOKEN_KEY);
    this._user.next(null);
    return of(true);
  }

  getJwtToken(): Observable<string> {
    let token: string = this.localStorage.get(TOKEN_KEY);
    return of(token);
  }

  getRefreshToken(): Observable<string> {
    let token: string = this.localStorage.get(REFRESH_TOKEN_KEY);
    return of(token);
  }

  isAuthenticated(): Observable<boolean> {
    return this.getJwtToken().pipe(map(token => !!token));
  }

  isTokenExpired() {
    return this.jwtHelper.isTokenExpired(this.localStorage.get(TOKEN_KEY), TOKEN_PRE_EXPIRY_IN_SEC);
  }

  refreshToken(): Observable<string> {
    return this.getRefreshToken().pipe(
      mergeMap(token => this.http.post<{token: string, refresh_token: string}>(`${apiUrl}/refresh`, token).pipe(
        map(result => {
          this.localStorage.set(TOKEN_KEY, result.token);
          this.localStorage.set(REFRESH_TOKEN_KEY, result.refresh_token);
          return result.token;
        }))
      ));
  }

  loadUserDetails() {
    const user: User = this.localStorage.get(USER_KEY);
    this._user.next(user);
  }

  updateUser(updates: {[key: string]: any}) {
    const user = Object.assign({}, this._user.value, updates);
    this._user.next(user);
    this.localStorage.set(USER_KEY, user);
  }

  forgotPassword(request: any) {
    return this.http.post(`${apiUrl}/forgot-password`, request);
  }

  resetForgottenPassword(request: any) {
    return this.http.post(`${apiUrl}/reset-forgotten-password`, request);
  }

  checkResetForgottenPasswordToken(request: any) {
    return this.http.post(`${apiUrl}/reset-forgotten-password/check`, request);
  }
}

export interface LoginResponse {
  token: string,
  refresh_token: string,
  device_token: string,
  user: User
}
