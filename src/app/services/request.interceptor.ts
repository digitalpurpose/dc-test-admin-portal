import {environment} from "../../environments/environment";
import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {mergeMap} from "rxjs/operators";
import {AuthService} from './auth.service';

const baseUrl = environment.apiUrl;

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

  constructor(private router: Router,
              private authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let url = request.url;
    if (url.startsWith(baseUrl) && url.indexOf("/auth/refresh") < 0) {
      return this.authService.getJwtToken().pipe(
        mergeMap((token) => {
          if (token) {
            request = request.clone({
              setHeaders: {
                Authorization: `Bearer ${token}`
              }
            });
          }
          return next.handle(request);
        })
      );
    } else {
      return next.handle(request)
    }
  }
}
