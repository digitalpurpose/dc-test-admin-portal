import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Page} from '../model/page';
import { UserDevice } from '../model/user-device';

@Injectable({
  providedIn: 'root'
})
export class UserDeviceService {

  private apiUrl = environment.apiUrl + '/user-device';

  constructor(private http: HttpClient) {
  }

  search(request): Observable<Page<UserDevice>> {
    return this.http.post<Page<UserDevice>>(`${this.apiUrl}/search`, request);
  }

  get(id: string): Observable<UserDevice> {
    return this.http.get<UserDevice>(`${this.apiUrl}/${id}`);
  }

}
