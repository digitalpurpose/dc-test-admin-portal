import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from './services/auth.guard';
import {UnauthorisedComponent} from './components/account/unauthorised/unauthorised.component';
import { LoginComponent } from './components/account/login/login.component';
import { ForgotPasswordComponent } from './components/account/forgot-password/forgot-password.component';
import { ResetForgottenPasswordComponent } from './components/account/reset-forgotten-password/reset-forgotten-password.component';
import { PortalComponent } from './components/portal/portal.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'portal'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent
  },
  {
    path: 'reset-forgotten-password/:confirmationToken',
    component: ResetForgottenPasswordComponent
  },
  {
    path: 'unauthorised',
    component: UnauthorisedComponent
  },
  { path: 'users', loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule) },
  { path: 'devices', loadChildren: () => import('./modules/user-devices/user-devices.module').then(m => m.UserDevicesModule) },
  {
    path: 'portal',
    canActivate: [AuthGuard],
    component: PortalComponent,
    children: [
      /*
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'HomeComponent',
      },
      */
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
