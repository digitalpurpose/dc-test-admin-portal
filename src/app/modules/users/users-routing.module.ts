import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { UsersComponent } from './users.component';
import { SearchUsersComponent } from './components/search/search-users.component';


const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: SearchUsersComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
