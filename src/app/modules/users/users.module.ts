import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UsersRoutingModule} from './users-routing.module';
import {UsersComponent} from './users.component';
import {SearchUsersComponent} from './components/search/search-users.component';
import {SharedModule} from '../shared/shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {faExclamationTriangle, faTimes} from '@fortawesome/free-solid-svg-icons';


@NgModule({
  declarations: [UsersComponent, SearchUsersComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ]
})
export class UsersModule {
  constructor(private faIconLibrary: FaIconLibrary) {
    this.faIconLibrary.addIcons(
      faExclamationTriangle, faTimes
    )
  }
}
