import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../../services/user.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {finalize} from 'rxjs/operators';
import {Page} from '../../../../model/page';
import {User} from '../../../../model/user';

@Component({
  selector: 'app-search-users',
  templateUrl: './search-users.component.html',
  styleUrls: ['./search-users.component.scss']
})
export class SearchUsersComponent implements OnInit {

  loading: boolean = true;
  error: any;
  form: FormGroup;
  result: Page<User> = null;

  constructor(private userService: UserService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      role: ['admin'],
      page: [0],
      size: 20,
      sort: [[{property: 'id', direction: 'DESC'}]]
    });
    this.form.valueChanges.subscribe(() => this.search());
    this.search();
  }

  private search() {
    const request = Object.assign(this.form.value);
    this.userService.search(request)
      .pipe(finalize(() => this.loading = false))
      .subscribe(
          (response) => this.result = response,
          (error) => this.error = error
      );
  }

  handlePageChange(page: number) {
    this.form.get('page').patchValue(page);
  }
}
