import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Page} from "../../../../../model/page";

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit, OnChanges {

  @Input() results: Page<any>;
  @Output() onPageChange = new EventEmitter<number>();

  page: number;
  hasPrev: boolean;
  hasNext: boolean;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.results) {
      this.page = this.results.number;
      this.hasPrev = this.page > 0;
      this.hasNext = this.page < this.results.totalPages - 1;
    } else {
      this.page = 0;
      this.hasPrev = false;
      this.hasNext = false;
    }
  }

  prevPage() {
    if (this.hasPrev) {
      this.onPageChange.emit(this.page - 1);
    }
  }

  nextPage() {
    if (this.hasNext) {
      this.onPageChange.emit(this.page + 1);
    }
  }
}
