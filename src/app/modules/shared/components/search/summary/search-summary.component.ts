import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Page} from '../../../../../model/page';

@Component({
  selector: 'app-search-summary',
  templateUrl: './search-summary.component.html',
  styleUrls: ['./search-summary.component.scss']
})
export class SearchSummaryComponent implements OnInit {

  @Input() results: Page<any>;
  @Output() onPageChange = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  pageChange(page: number) {
    this.onPageChange.emit(page);
  }
}
