import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ChangePasswordModalComponent } from 'src/app/components/account/change-password/modal/change-password-modal.component';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  collapsed: boolean = true;
  selectedTab: string;

  constructor(public authService: AuthService,
              private router: Router,
              private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  logout() {
    this.authService.logout().subscribe(
      () => this.router.navigate(['/login']),
      (error) => console.error(error)
    );
  }

  isActive(link: string): boolean {
    return this.router.url.split('/')[2] == link;
  }

  changePassword() {
    this.modalService.open(ChangePasswordModalComponent);
  }

}
