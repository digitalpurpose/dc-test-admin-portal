import {Pipe, PipeTransform} from '@angular/core';
import {JsonPipe} from '@angular/common';

@Pipe({
  name: 'prettyJson'
})
export class PrettyJsonPipe implements PipeTransform {

  constructor(private jsonPipe: JsonPipe) {
  }

  transform(value: string): any {
    const json = this.jsonPipe.transform(value);
    if (typeof(json) === 'string' && json.match(/^["']/)) {
      return json.substring(1, json.length - 1);
    }

    return json;
  }
}
