import {Pipe, PipeTransform} from '@angular/core';
import { User } from 'src/app/model/user';

@Pipe({
  name: 'user'
})
export class UserPipe implements PipeTransform {

  transform(user: User, ...args: unknown[]): unknown {
    const arg = args.length ? args[0] : null;
    switch (arg) {
      case 'name':
        return user.firstName + ' ' + user.lastName;
    }
    return '';
  }

}
