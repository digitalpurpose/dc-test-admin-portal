import {Directive, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "ASC"',
    '[class.desc]': 'direction === "DESC"',
    '(click)': 'rotate()'
  }
})
export class SortableTableHeaderDirective implements OnInit {

  @HostBinding('style.cursor') cursor = 'pointer';

  @Input() sortable: string | string[] = '';
  @Input() direction: SortDirection = '';
  @Input() reactiveForm: FormGroup;

  @Output() onSortChanged = new EventEmitter<SortOrder[]>();

  private defaultSort: any;

  private ngDestroy$ = new Subject();

  constructor() { }

  ngOnInit() {
    if (this.reactiveForm) {
      let sort = this.reactiveForm.get('sort');
      if (sort) {
        this.defaultSort = sort ? sort.value : null;
      } else {
        this.reactiveForm.addControl('sort', new FormControl(null));
        sort = this.reactiveForm.get('sort');
      }
      sort.valueChanges
        .pipe(takeUntil(this.ngDestroy$))
        .subscribe(s => {
          const props = s.map(s => s.property);
          const thisProps = this.sortable instanceof Array ? this.sortable : [this.sortable];
          if (props.length != thisProps.length || !this.arraysContainsSameContents(props, thisProps)) {
            this.direction = '';
          }
        })
    }
  }

  ngOnDestroy() {
    this.ngDestroy$.next(true);
    this.ngDestroy$.complete();
  }

  rotate() {
    this.direction = rotate[this.direction];
    let newSort = [];
    // Check if the sort is being applied on this header
    if (this.direction !== '') {
      // Check if the property is an array or a string
      if (this.sortable instanceof Array) {
        this.sortable.forEach(prop => newSort.push({direction: this.direction, property: prop}));
      } else {
        newSort.push({direction: this.direction, property: this.sortable});
      }
    } else {
      // No sort applied - apply a sensible default
      newSort = this.defaultSort || [{property: 'id', direction: 'ASC'}];
    }
    /* Update the form and trigger search */
    if (this.reactiveForm) {
      let sort = this.reactiveForm.get('sort');
      if (sort) {
        sort.patchValue(newSort)
      } else {
        this.reactiveForm.addControl('sort', new FormControl(newSort));
      }
    }
    /* Emit the event in the case that the reactiveForm */
    this.onSortChanged.emit(newSort);
  }

  private arraysContainsSameContents(arr1, arr2): boolean {
    for (let i = 0; i < arr1.length; i++) {
      if (arr1[i] != arr2[i]) {
        return false;
      }
    }
    return true;
  }

}

export type SortDirection = 'ASC' | 'DESC' | '';
const rotate: {[key: string]: SortDirection} = { 'ASC': 'DESC', 'DESC': '', '': 'ASC' };

export interface SortOrder {
  property: string;
  direction: string;
}
