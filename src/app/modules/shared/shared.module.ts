import {NgModule} from '@angular/core';
import {CommonModule, JsonPipe} from '@angular/common';
import {RouterModule} from "@angular/router";
import {UserPipe} from './pipes/user.pipe';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {faChevronLeft, faChevronRight} from '@fortawesome/free-solid-svg-icons';
import {HumanisePipe} from './pipes/humanise.pipe';
import {PrettyJsonPipe} from "./pipes/pretty-json.pipe";
import { ErrorComponent } from './components/error/error.component';
import { LoadingComponent } from './components/loading/loading.component';
import { PaginationComponent } from './components/search/pagination/pagination.component';
import { SearchSummaryComponent } from './components/search/summary/search-summary.component';
import { SortableTableHeaderDirective } from './directive/sortable-table-header.directive';
import { HeaderComponent } from './components/header/header.component';
import {
  NgbModule,
} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    UserPipe,
    HumanisePipe,
    PrettyJsonPipe,
    HeaderComponent,
    ErrorComponent,
    LoadingComponent,
    PaginationComponent,
    SearchSummaryComponent,
    SortableTableHeaderDirective,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule,
    NgbModule
  ],
  exports: [
    UserPipe,
    HumanisePipe,
    PrettyJsonPipe,
    HeaderComponent,
    ErrorComponent,
    LoadingComponent,
    PaginationComponent,
    SearchSummaryComponent,
    SortableTableHeaderDirective,
  ],
  providers: [
    [JsonPipe]
  ]
})
export class SharedModule {
  constructor(private faIconLibrary: FaIconLibrary) {
    this.faIconLibrary.addIcons(
      faChevronLeft, faChevronRight,
    )
  }
}
