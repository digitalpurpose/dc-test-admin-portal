import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserDevicesComponent } from './user-devices.component';
import { SearchDevicesComponent } from './components/search/search-devices.component';
import { ViewDeviceComponent } from './components/view/view-device.component';

const routes: Routes = [
  {
    path: '',
    component: UserDevicesComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: SearchDevicesComponent
      },
      {
        path: ':id',
        component: ViewDeviceComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserDevicesRoutingModule { }
