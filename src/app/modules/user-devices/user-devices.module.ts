import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {SharedModule} from '../shared/shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {faExclamationTriangle, faTimes} from '@fortawesome/free-solid-svg-icons';
import { UserDevicesRoutingModule } from './user-devices-routing.module';
import { UserDevicesComponent } from './user-devices.component';
import { SearchDevicesComponent } from './components/search/search-devices.component';
import { ViewDeviceComponent } from './components/view/view-device.component';


@NgModule({
  declarations: [
    UserDevicesComponent,
    SearchDevicesComponent,
    ViewDeviceComponent
  ],
  imports: [
    CommonModule,
    UserDevicesRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ]
})
export class UserDevicesModule { 
  constructor(private faIconLibrary: FaIconLibrary) {
    this.faIconLibrary.addIcons(
      faExclamationTriangle, faTimes
    )
  }
   
}
