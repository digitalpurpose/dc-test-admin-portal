import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MfdService } from 'src/app/services/mfd.service';
import { finalize } from 'rxjs/operators';
import { Mfd } from 'src/app/model/mfd';
import { UserDevice } from 'src/app/model/user-device';
import { UserDeviceService } from 'src/app/services/user-device.service';
import { forkJoin } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-view-device',
  templateUrl: './view-device.component.html',
  styleUrls: ['./view-device.component.scss']
})
export class ViewDeviceComponent implements OnInit {

  loading: boolean = true;
  error: any;

  deviceId: string;  
  results: Mfd[];
  device: UserDevice;

  constructor(private mfdService: MfdService,
              private userDeviceService: UserDeviceService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.deviceId = params.id;
      this.loadMfds(this.deviceId);
    });
  }

  private loadMfds(identifier: string) {
    this.error = null;
    this.loading = true;

    forkJoin([
      this.mfdService.get(identifier),
      this.userDeviceService.get(identifier)
    ])
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        (responses) => {
          this.results = responses[0];
          this.device = responses[1];
        },
        (error) => this.error = error
      );
  }

  week(mfd) {
    const date = moment(mfd.mfdDate); 
    let from = moment(this.device.createdDate);
    let to = moment(this.device.createdDate).add(1, 'week');
    
    for (let i = 1; i <= 5; i++) {
      if (date.isSameOrAfter(from) && date.isBefore(to)) {
         return i;
      } else {
         from = moment(this.device.createdDate).add(i, 'weeks');
         to = moment(this.device.createdDate).add(i+1, 'weeks');
      }
    }
    return 9;
  }

}
