import { Component, OnInit } from '@angular/core';
import { UserDevice } from 'src/app/model/user-device';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Page } from 'src/app/model/page';
import { UserDeviceService } from 'src/app/services/user-device.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-search-devices',
  templateUrl: './search-devices.component.html',
  styleUrls: ['./search-devices.component.scss']
})
export class SearchDevicesComponent implements OnInit {

  loading: boolean = true;
  error: any;
  form: FormGroup;
  result: Page<UserDevice> = null;

  constructor(private userDeviceService: UserDeviceService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      page: [0],
      size: 20,
      sort: [[{property: 'createdDate', direction: 'DESC'}]]
    });
    this.form.valueChanges.subscribe(() => this.search());
    this.search();
  }

  private search() {
    const request = Object.assign(this.form.value);
    this.userDeviceService.search(request)
      .pipe(finalize(() => this.loading = false))
      .subscribe(
          (response) => this.result = response,
          (error) => this.error = error
      );
  }

  handlePageChange(page: number) {
    this.form.get('page').patchValue(page);
  }

}
