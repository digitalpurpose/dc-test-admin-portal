export interface Page<T> {
  content: T[];
  hasNext: boolean;
  hasPrevious: boolean;
  number: number;
  numberOfElements: number;
  totalElements: number;
  totalPages: number;
}
