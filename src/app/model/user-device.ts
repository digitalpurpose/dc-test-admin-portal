export interface UserDevice {
  id: number;
  identifier: string;
  createdDate: Date;
  averageMfdPerWeek: number;
  totalMfd: number;
}
