import {AuditableRecord} from './auditable-record';
import {Status} from './status';

export interface User extends AuditableRecord {
  id: number;
  status: Status;
  email: string;
  firstName: string;
  lastName: string;
  lastLogin: Date;
}
