export enum Status {
  pending = 'pending',
  active = 'active',
  inactive = 'inactive',
  locked = 'locked',
  deleted = 'deleted'
}
