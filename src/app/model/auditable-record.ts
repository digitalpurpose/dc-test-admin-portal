export interface AuditableRecord {
  createdOn: string;
  createdBy: number;
  updatedOn: string;
  updatedBy: number;
}
