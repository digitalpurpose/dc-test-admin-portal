export const environment = {
  production: true,
  apiUrl: 'https://dctestapp-server.digitalpurpose.com.au/api/admin',
};
